const fs = require('fs');

/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt ---
        2. Convert the content to uppercase & write to a new file. --
        Store the name of the new file in filenames.txt --
        3. Read the new file and convert it to lower case. --
        Then split the contents into sentences.  --
        Then write it to a new file. -- 
        Store the name of the new file in filenames.txt --
        4. Read the new files, sort the content, 
        write it out to a new file. 
        Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and 
        delete all the new files that are mentioned in that list simultaneously.
*/

function filePLay(dataFilePath, filePath, callback) {
    fs.readFile(dataFilePath, 'utf-8', (err, data) => {
        if (err) {
            callback(err, null);
        } else {
            callback(null, '${dataFilePath} file has been read');
            fs.writeFile('uppercase.txt', data.toUpperCase(), (err) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, 'converted to uppercase');
                    fs.writeFile('filenames.txt', 'uppercase.txt', (err) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, 'stored filename in filename.txt');
                            fs.readFile('uppercase.txt', 'utf-8', (err, uppercaseData) => {
                                if (err) {
                                    callback(err, null);
                                } else {
                                    callback(null, 'uppercase file has been read');
                                    let toLowerCase = uppercaseData.toLowerCase().split(".").join('/n');
                                    fs.writeFile('lowercase.txt', toLowerCase, (err) => {
                                        if (err) {
                                            callback(err, null);
                                        } else {
                                            callback(null, 'converted to LowerCase and saved as lowercase.txt');
                                            fs.appendFile('filenames.txt', '\nlowercase.txt', (err) => {
                                                if (err) {
                                                    callback(err, null);
                                                } else {
                                                    // 4. Read the new files, sort the content,
                                                    //     write it out to a new file. 
                                                    //     Store the name of the new file in filenames.txt
                                                    callback(null, 'appended LowerCase.txt to filenames.txt');
                                                    fs.readFile('lowercase.txt', 'utf-8', (err, lowercaseData) => {
                                                        if (err) {
                                                            callback(err, null);
                                                        } else {
                                                            callback(null, 'lowercase file has been read');
                                                            lowercaseData = lowercaseData.split('/n').sort().join('/n');
                                                            fs.writeFile('sorted.txt', lowercaseData, (err) => {
                                                                if (err) {
                                                                    callback(err, null);
                                                                } else {
                                                                    callback(null, 'sorted file has been stored');
                                                                    fs.appendFile('filenames.txt', '\nsorted.txt', (err) => {
                                                                        if (err) {
                                                                            callback(err, null);
                                                                        } else {
                                                                            callback(null, 'appended sorted.txt to filenames.txt');
                                                                            // 5. Read the contents of filenames.txt and 
                                                                            // delete all the new files that are mentioned in that list simultaneously.
                                                                            fs.readFile('filenames.txt', 'utf-8', (err, filedata) => {
                                                                                if (err) {
                                                                                    callback(err, null);
                                                                                } else {
                                                                                    console.log("begin to read filename.txt");
                                                                                    let files = filedata.split('\n');
                                                                                    files.forEach(file => {
                                                                                        file = "./" + file;
                                                                                        fs.unlink(file, (err) => {
                                                                                            if (err) {
                                                                                                callback(err, null);
                                                                                            } else {
                                                                                                callback(null, `${file} deleted`);
                                                                                            }
                                                                                        })
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}

module.exports = filePLay;