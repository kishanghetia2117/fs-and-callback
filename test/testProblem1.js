let problem1 = require('../problem1.js');
let dirPath = './data/random';

const jsonFilePath = [
    "./data/random/one.json",
    "./data/random/two.json",
];

const callback = (err, data) => {
    if (err) {
        console.log(err);
    } else {
        console.log(data);
    }
}

problem1(dirPath, jsonFilePath, callback);


