let problem2 = require('../problem2.js');

let dataFilePath = './data/lipsum.txt';
let filePath = "./data/filePlayed";


const callback = (err, data) => {
    if (err) {
        console.log(err);
    } else {
        console.log(data);
    }
}
problem2(dataFilePath, filePath, callback);