let fs = require('fs');

function jsonCreateDestroy(dirName, filePath, callback) {
    fs.mkdir(dirName, (err) => {
        if (err) {
            callback(err,null);
        } else {
            callback(null, `dir ${dirName} has been created`);
            filePath.forEach(file => {
                fs.writeFile(file, "data", (err) => {
                    if (err) {
                        callback(err,null);
                    } else {
                        callback(null, `${file} has been created`);
                        fs.unlink(file, (err) => {
                            if (err) {
                                callback(err,null);
                            } else {
                                callback(null, `${file} destroyed sucessfully`);
                            }
                        })
                    }
                })
            })
        }
    })
}

module.exports = jsonCreateDestroy;



